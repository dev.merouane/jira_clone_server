import 'dotenv/config';
import express from 'express';

const initializeExpress = (): void => {
  const app = express();

  app.use(express.json());
  app.use(express.urlencoded({extended: true})); 
  
  app.listen(process.env.PORT || 3000);
};

const initializeApp = async (): Promise<void> => {
  await initializeExpress();
};

initializeApp();
